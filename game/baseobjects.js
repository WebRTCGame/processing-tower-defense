
var Square = function (gx, gy, color) {
    var square = new Object();
    Object.extend(square, InertDrawable);
    square.type = "";
    square.gx = gx;
    square.gy = gy;
    square.x = grid_to_pixel(gx);
    square.y = grid_to_pixel(gy);
    var mid = center_of_square(gx, gy);
    square.x_mid = mid.x;
    square.y_mid = mid.y;
    square.color = color;
    square.draw = function () {};
    assign_to_depth(square, SET.zIndex.square);
    return square;
};

var ExitSquare = function (gx, gy) {
    var square = Square(gx, gy, SET.exit_color);
    square.type = "exit";
    square.draw = function () {
        context.save();
        context.fillStyle = SET.exit_color;
        draw_square_in_grid(this.gx, this.gy);
        var pos = grid_to_pixel(this.gx, this.gy);
        var h = SET.half_pixels_per_square;
        var l = SET.pixels_per_square;
        context.save();
        context.lineWidth = 2;
        context.strokeStyle = "rgba(0,0,0,1)";
        context.beginPath();
        context.moveTo(pos.x + h, pos.y + h);
        context.arc(pos.x + h, pos.y + h, (l - 1) / 2, 0, Math.PI * 2, false);
        context.closePath();
        context.stroke();
        context.fill();
        context.restore();
        context.restore();
    };
    return square;
};