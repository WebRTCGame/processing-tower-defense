

// PARTICLE SYSTEM - receives number and vector

var ParticleSystem = function (num, v, _life, _maxSize) {
	//Object.extend(this, inertDrawable);
	this.particles = [];
	this.origin = new Vector2D(v.x, v.y); //v;
	//this.is_dead = false;
	this.life = _life;
	var maxSize = _maxSize;
	this.hasLifetime = false;
	this.lifeTime = 30;
	
	for (var i = 0; i < num; i++) {
		if (this.particles.length < num) {
			this.particles.push(new Particle(v.x, v.y, _life, maxSize));
		};
		
	};
	
	// running the particle system
	this.update = function () {
		if (this.hasLifetime) {
			this.lifeTime -= 1;
		};
		this.addParticle(this.origin.x, this.origin.y);
		
		// iterate through particles
		for (var i = this.particles.length - 1; i >= 0; i--) {
			var p = this.particles[i];
			//p.run();
			p.update();
			p.render();
			
			if (p.is_dead()) {
				this.particles.splice(i, 1);
			}
		}
	}
	this.draw = function () {
		//SET.addLight(this.x,this.y,50);
		for (var i = this.particles.length - 1; i >= 0; i--) {
			var p = this.particles[i];
			p.render();
		}
	};
	// adding a particle
	this.addParticle = function (_x, _y) {
		this.particles.push(new Particle(_x, _y, this.life, maxSize));
	}
	this.is_dead = function () {
		return (this.particles.length < 1 || this.lifeTime <= 0);
	}
};

// a simple Particle class

var Particle = function (_x, _y, _life, _maxSize) {
	
	var whateva = _life;
	this.acc = new Vector2D(0, 0.01);
	this.vel = new Vector2D(Math.random() * 2 - 1, Math.random() * 2 - 2);
	this.loc = new Vector2D(_x, _y);
	this.timer = _life;
	this.maxSize = _maxSize;
	
	this.update = function () {
		// particle loop: acceleration, velocity, and location update
		if (this.timer > -1) {
			this.vel.add(this.acc);
			this.loc.add(this.vel);
			this.timer -= 1;
			//this.r = this.timer;
			if (this.loc.x > SET.width) {
				this.timer = -1
			};
			if (this.loc.x < 0) {
				this.timer = -1
			};
			if (this.loc.y > SET.height) {
				this.timer = -1
			};
			if (this.loc.y < 0) {
				this.timer = -1
			};
		};
		
	}
	
	this.render = function () {
		if (this.timer > -1) {
			
			context.save();
			
			var percentageLife = ((whateva - this.timer) / whateva) * 100;
			var scaleup = 255 / (percentageLife / 10);
			context.fillStyle = color(scaleup, 0, 0, 0.25);
			context.beginPath();
			context.moveTo(this.loc.x, this.loc.y);
			var psize = this.maxSize / (percentageLife / 10);
			
			if (psize <= 1) {
				psize = 1
			};
			if (psize > this.maxSize) {
				psize = this.maxSize
			};
			SET.addLight(this.loc.x,this.loc.y,psize*4);
			context.arc(this.loc.x, this.loc.y, psize, 0, Math.PI * 2, true);
			context.closePath();
			context.fill();
			context.restore();
		};
	};
	
	this.is_dead = function () {
		return this.timer <= 0 ? true : false;
	}
};
