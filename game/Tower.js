var Tower = function (settings) {
	var tower = Square(settings.gx, settings.gy, settings.color);
	Object.extend(tower, settings);
	tower.set_range = function (range) {
		tower.range = range;
		tower.prange = range * SET.pixels_per_square;
	};
	tower.account_for_terrain = function () {
		var terrain = get_terrain_at(this.gx, this.gy);
		this.damage = this.damage * terrain.tower_damage_modifier;
		this.set_range(this.range * terrain.tower_range_modifier);
		this.reload_rate = this.reload_rate * terrain.tower_frequency_modifier;
	};
	tower.type = settings.type;
	console.log(tower.type);
	tower.set_range(3.5);
	tower.angle = 0;
	tower.damage = 10;
	tower.cost = 10;
	tower.level = 0;
	var mid = center_of_square(tower.gx, tower.gy);
	tower.x_mid = mid.x;
	tower.y_mid = mid.y;
	tower.fired_at = 0;
	tower.reload_rate = 100;
	tower.Atargetx = -10;
	tower.Atargety = -10;
	tower.upgrade_cost = 100;
	tower.shots_per_volley = 3;
	tower.shots_left_in_volley = tower.shots_per_volley;
	tower.pause_after_volley = 2000;
	tower.finish_reload_at = 0;
	tower.reloading = false;
	tower.fire_next_at = 0;
	tower.sale_value = 50;

	tower.account_for_terrain();

	tower.bulletfunc = settings.bulletfunc;

	tower.attack = function (creep) {
		assign_to_depth(tower.bulletfunc(creep), SET.zIndex.bullet);
		tower.shots_left_in_volley--;
		tower.fire_next_at = SET.now + tower.reload_rate;
		if (tower.shots_left_in_volley < 1) {
			tower.reloading = true;
			tower.finish_reload_at = SET.now + tower.pause_after_volley;
		}
	};

	tower.weapon_ready = function () {
		/*if (SET.now - tower.fired_at > tower.reload_rate) {
		tower.fired_at = SET.now;
		return true;
		}
		return false;
		 */
		if (tower.reloading && tower.finish_reload_at < SET.now) {
			tower.shots_left_in_volley = tower.shots_per_volley;
			tower.reloading = false;
		}
		if (!tower.reloading && tower.fire_next_at < SET.now) {
			return true;
		}
		return false;
	};

	tower.update = function () {

		if (SET.rendering_groups[SET.zIndex.creep].length !== 0) {

			var closest_creep = _.find(SET.rendering_groups[SET.zIndex.creep], function (creep) {
					return dist(tower.x_mid, tower.y_mid, creep.x, creep.y) < tower.prange;
				});

			if (closest_creep) {
				this.Atargetx = closest_creep.x;
				this.Atargety = closest_creep.y;
				this.angle = Math.atan2((this.Atargety - this.y_mid), (this.Atargetx - this.x_mid));
				if (tower.weapon_ready()) {
					tower.attack(closest_creep);
				};
			};

		};
	};

	tower.sell = function () {
		SET.gold += Math.floor(this.sale_value * 0.75);
		this.is_dead = function () {
			return true;
		};
		SET.grid_cache_at(this.gx, this.gy).tower = undefined;

		unselect();
	};

	tower.applyUpgradeModifier = function () {
		this.damage = Math.floor(this.damage * 2.5);
		this.set_range(this.range + 0.5);
		this.speed *= 1.1;
	};

	tower.upgrade = function () {
		if (SET.gold >= this.upgrade_cost) {
			SET.gold -= this.upgrade_cost;
			this.sale_value = Math.floor(this.sale_value + this.upgrade_cost);
			this.upgrade_cost = Math.floor(this.upgrade_cost * 1.5);
			this.level = this.level + 1;
			this.applyUpgradeModifier();
			unselect();
			SET.state = new TowerSelectMode();
			SET.state.set_up(this.x_mid, this.y_mid);
		} else
			error("You don't have enough gold to upgrade, you need " + (this.upgrade_cost - SET.gold) + " more.");
	};

	tower.upgradeDamage = function () {
		tower.damage = tower.upgradeparticular(tower.damage);
	};
	tower.upgradeRate = function () {
		tower.reload_rate = tower.upgradeparticular(tower.reload_rate);
	};
	tower.upgradeShots = function () {
		tower.shots_per_volley = tower.upgradeparticular(tower.shots_per_volley);
	};
	tower.upgradeRange = function () {
		tower.set_range(tower.upgradeparticular(tower.range));
	};

	tower.upgradeparticular = function (particular) {

		var upgradecostdiv3 = Math.round(this.upgrade_cost / 3);
		if (SET.gold >= (upgradecostdiv3)) {
			SET.gold -= (upgradecostdiv3);
			this.sale_value = Math.floor(this.sale_value + (upgradecostdiv3));
			this.upgrade_cost = Math.floor(this.upgrade_cost + (upgradecostdiv3 * 1.5));
			this.level = this.level + 0.3;
			particular = Math.round(Math.floor(particular * 1.5));
			unselect();
			SET.state = new TowerSelectMode();
			SET.state.set_up(this.x_mid, this.y_mid);
		} else {
			error("You don't have enough gold to upgrade, you need " + ((upgradecostdiv3) - SET.gold) + " more.");
		}

		return particular;
	};

	tower.display_stats = function () {
		WIDGETS.tower_typeval = this.type;
		WIDGETS.tower_type.innerHTML = this.type;
		WIDGETS.tower_rangeval = this.range;
		WIDGETS.tower_range.innerHTML = this.range;
		WIDGETS.tower_damageval = this.damage;
		WIDGETS.tower_damage.innerHTML = this.damage;
		WIDGETS.tower_rateval = this.reload_rate;
		WIDGETS.tower_rate.innerHTML = this.reload_rate;
		WIDGETS.tower_sellval = Math.floor(this.sale_value * 0.75);
		WIDGETS.tower_sell.innerHTML = Math.floor(this.sale_value * 0.75);
		WIDGETS.tower_upgradeval = Math.floor(this.upgrade_cost);
		WIDGETS.tower_upgrade.innerHTML = Math.floor(this.upgrade_cost);
		WIDGETS.tower_levelval = this.level;
		WIDGETS.tower_level.innerHTML = this.level;
		WIDGETS.tower_sell_button.innerHTML = "Sell";
		WIDGETS.tower_upgrade_button.innerHTML = "<u>U</u>pgrade";
		WIDGETS.tower_upgrade_damage.innerHTML = "<u>U</u>pgrade damage " + Math.floor(this.upgrade_cost / 3);
		WIDGETS.tower_upgrade_range.innerHTML = "<u>U</u>pgrade range " + Math.floor(this.upgrade_cost / 3);
		WIDGETS.tower_upgrade_rate.innerHTML = "<u>U</u>pgrade rate " + Math.floor(this.upgrade_cost / 3);

		WIDGETS.tower_upgrade_button.onclick = function () {
			tower.upgrade();
		};

		WIDGETS.tower_upgrade_damage.onclick = function () {
			//tower.upgrade();
			//console.log("upgrade damage");
			tower.upgradeDamage();
			//console.log("finished upgrade damage");
		};
		WIDGETS.tower_upgrade_range.onclick = function () {
			//tower.upgrade();
			//console.log("upgrade range");
		};
		WIDGETS.tower_upgrade_rate.onclick = function () {
			//tower.upgrade();
			//console.log("upgrade rate");
		};
		WIDGETS.tower_sell_button.onclick = function () {
			tower.sell();
			reset_pathfinding();

		};

	};
	tower.draw = function () {
		//SET.addLight(this.x_mid,this.y_mid,this.prange);
		context.save();
		context.shadowOffsetX = 5;
		context.shadowOffsetY = 5;
		context.shadowColor = "black";
		context.shadowBlur = 10;
		context.fillStyle = this.color;
		var pos = grid_to_pixel(this.gx, this.gy);
		var h = SET.half_pixels_per_square;
		var l = SET.pixels_per_square;

		context.save();
		context.lineWidth = 2;
		context.strokeStyle = "rgba(0,0,0,1)";
		context.beginPath();
		context.moveTo(pos.x + h, pos.y + h);
		context.arc(pos.x + h, pos.y + h, (l - 1) / 2, 0, Math.PI * 2, false);
		context.closePath();
		context.stroke();
		context.fill();
		context.restore();
		var barrelLength = 20;
		var ix = this.x_mid + barrelLength * Math.cos(this.angle);
		var iy = this.y_mid + barrelLength * Math.sin(this.angle);
		context.lineWidth = 6;
		context.strokeStyle = "rgba(0,0,0,1)";
		context.beginPath();
		context.moveTo(this.x_mid, this.y_mid);
		context.lineTo(ix, iy);
		context.closePath();
		context.stroke();
		context.lineWidth = 2;
		context.strokeStyle = "rgba(100,100,100,1)";
		context.beginPath();
		context.moveTo(this.x_mid, this.y_mid);
		context.lineTo(ix, iy);
		context.closePath();
		context.stroke();
		context.restore();

		context.save();
		context.lineWidth = 2;
		context.strokeStyle = "#FF0000";
		context.lineWidth = 2;
		context.strokeStyle = "rgba(0,0,0,1)";
		context.beginPath();
		context.moveTo(this.Atargetx, this.Atargety);
		context.arc(this.Atargetx, this.Atargety, 10, 0, Math.PI * 2, false);
		context.closePath();
		context.stroke();
		context.fill();
		context.restore();

	}
	assign_to_depth(tower, SET.zIndex.tower);
	return tower;
};

var MissileTower = function (gx, gy) {
	return Tower({
		gx : gx,
		gy : gy,
		color : color(250, 150, 50),
		type : "Missile Tower",
		bulletfunc : function (creep) {
			return Missile(this, creep);
		}
	});
};

var LaserTower = function (gx, gy) {
	return Tower({
		gx : gx,
		gy : gy,
		color : color(90, 150, 50),
		type : "Laser Tower",
		bulletfunc : function (creep) {
			return Laser(this, creep);
		}
	});
};

var CannonTower = function (gx, gy) {
	return Tower({
		gx : gx,
		gy : gy,
		color : color(100, 120, 140),
		type : "Cannon Tower",
		bulletfunc : function (creep) {
			return CannonBall(this, creep);
		}
	});
};

var GatlingTower = function (gx, gy) {
	return Tower({
		gx : gx,
		gy : gy,
		color : color(250, 250, 50),
		type : "Gatling Tower",
		bulletfunc : function (creep) {
			return Bullet(this, creep);
		}
	});

};
