
function processing() {

	var p = {};
	
	var mousePressed = false;
	var keyPressed = false;
	
	p.unselect = function () {
		if (SET.state)
			SET.state.tear_down();
		SET.state = undefined;
		$('').trigger("no_mode");
	};
	
	p.pause = function () {
		paused = !paused;
		pause_resume();
		$('#pause_button').html(paused ? "Resume" : "Pause");
	};
	/* illustrating to the user what mode they're in*/
	p.mode_signifiers = {
		'BuildLaserTowerMode' : 'laser_button',
		'BuildGatlingTowerMode' : 'gatling_button',
		'BuildCannonTowerMode' : 'cannon_button',
		'BuildMissileTowerMode' : 'missile_button',
		'AimBombMode' : 'bomb_button',
		'PauseMode' : 'pause_button'
	};
	
	p.pmouseX = 0;
	p.pmouseY = 0;
	p.mouseX = 0;
	p.mouseY = 0;
	p.mouseButton = 0;
	
	p.mouseDragged = undefined;
	p.mouseMoved = undefined;
	p.mousePressed = undefined;
	p.mouseReleased = undefined;
	p.keyPressed = undefined;
	p.keyReleased = undefined;
	
	p.mouse_pos = function () {
		return {
			x : p.mouseX,
			y : p.mouseY
		};
	};
	
	p.previous_mouse_pos = function () {
		return {
			x : p.pmouseX,
			y : p.pmouseY
		};
	};
	
	p.init = function init() {
		
		attach(canvas, "mousemove", function (e) {
			var scrollX = window.scrollX != null ? window.scrollX : window.pageXOffset;
			var scrollY = window.scrollY != null ? window.scrollY : window.pageYOffset;
			p.pmouseX = p.mouseX;
			p.pmouseY = p.mouseY;
			p.mouseX = e.clientX - canvas.offsetLeft + scrollX;
			p.mouseY = e.clientY - canvas.offsetTop + scrollY;
			
			if (p.mouseMoved) {
				p.mouseMoved();
			}
			
			if (mousePressed && p.mouseDragged) {
				p.mouseDragged();
			}
		});
		
		attach(canvas, "mousedown", function (e) {
			mousePressed = true;
			p.mouseButton = e.which;
			
			if (typeof p.mousePressed == "function") {
				p.mousePressed();
			} else {
				p.mousePressed = true;
			}
		});
		
		attach(canvas, "contextmenu", function (e) {
			e.preventDefault();
			e.stopPropagation();
		});
		
		attach(canvas, "mouseup", function (e) {
			mousePressed = false;
			
			if (typeof p.mousePressed != "function") {
				p.mousePressed = false;
			}
			
			if (p.mouseReleased) {
				p.mouseReleased();
			}
		});
		
		attach(document, "keydown", function (e) {
			keyPressed = true;
			
			p.key = e.keyCode + 32;
			
			if (e.shiftKey) {
				p.key = String.fromCharCode(p.key).toUpperCase().charCodeAt(0);
			}
			
			if (typeof p.keyPressed == "function") {
				p.keyPressed();
			} else {
				p.keyPressed = true;
			}
		});
		
		attach(document, "keyup", function (e) {
			keyPressed = false;
			
			if (typeof p.keyPressed != "function") {
				p.keyPressed = false;
			}
			
			if (p.keyReleased) {
				p.keyReleased();
			}
		});
		
		function attach(elem, type, fn) {
			if (elem.addEventListener) {
				elem.addEventListener(type, fn, false);
			} else {
				elem.attachEvent("on" + type, fn);
			};
		};
	};
	
	return p;
};

