
var Bullet = function (tower, target) {
	var obj = new Object();
	Object.extend(obj, Weapon(tower, target));
	obj.size = 5;
	obj.color = color(255, 255, 255);

	obj.draw = function () {
		//SET.addLight(this.x, this.y, this.size * 3);
		context.save();
		context.lineWidth = 2;
		context.strokeStyle = "rgba(0,0,0,1)";
		context.beginPath();
		context.moveTo(this.x, this.y);
		context.arc(this.x, this.y, this.size, 0, Math.PI * 2, false);
		context.closePath();
		context.stroke();
		context.fill();
		context.restore();
	}
	return obj;
};

var Laser = function (tower, target) {
	var obj = new Object();
	Object.extend(obj, Weapon(tower, target));
	obj.color = color(0, 0, 0, .5);
	obj.speed = random(10) * 2 + 5;
	obj.heatseek = true;
	obj.showParticles = false;
	obj.shoulddie = 0;

	obj.draw = function () {
		//SET.addLight(this.x, this.y, 10);
		/*
		context.save()
		context.strokeStyle = "rgba(0,0,255,0)";
		context.lineWidth = 6;
		context.beginPath();
		context.moveTo(obj.x, obj.y);
		context.lineTo(tower.x_mid, tower.y_mid);
		context.closePath();
		context.stroke();
		context.strokeStyle = "rgba(255,255,255,1)";
		context.lineWidth = 2;
		context.beginPath();
		context.moveTo(obj.x, obj.y);
		context.lineTo(tower.x_mid, tower.y_mid);
		context.closePath();
		context.stroke();
		context.restore()
		 */
		var bullets = SET.rendering_groups[SET.zIndex.bullet];
		var l = bullets.length;
		//console.log(l);
		var neighborscounter = 0;

		for (var i = 0; i < l; i++) {
			var bullet = bullets[i];
			if (this !== bullet) {
				var d = Math.floor(dist(this.x, this.y, bullet.x, bullet.y));
				if (d <= 25) {
					context.save();
					context.fillStyle = this.color;
					context.lineWidth = 6;
					//context.translate(this.x, this.y);
					//context.rotate(this.angle);
					context.beginPath();
					context.moveTo(obj.x, obj.y);
					context.lineTo(bullet.x, bullet.y);
					context.closePath();
					context.strokeStyle = "rgba(255,0,0,0.25)";
					context.stroke();
					context.strokeStyle = "rgba(0,0,0,1)";
					context.restore();
					neighborscounter = neighborscounter + 1;
					//console.log(neighborscounter);
				};
			};

		};
		if (neighborscounter <= 1) {
			this.shoulddie = this.shoulddie + 1;
			//console.log(this.shoulddie);
		};
		if (this.shoulddie == 10) {
			//console.log("shoulddie=10");
			this.is_dead = function () {
				return true;
			};
		};
		/*
		context.save();
		context.fillStyle = this.color;
		context.lineWidth = 1;
		context.translate(this.x, this.y);
		context.rotate(this.angle);
		context.beginPath();
		context.moveTo(-4, -4);
		context.lineTo(8, 0);
		context.lineTo(-4, 4);
		context.closePath();
		context.fill();
		context.stroke();
		context.restore();
		 */
	};
	return obj;
};

var Missile = function (tower, target) {
	var obj = new Object();
	Object.extend(obj, Weapon(tower, target));
	obj.size = 10;
	obj.color = color(255, 0, 0);
	obj.splash_range = 50.0;
	obj.speed = 20;
	obj.heatseek = true;
	obj.draw = function () {
		//SET.addLight(this.x, this.y, this.size * 3);
		context.save();
		context.fillStyle = this.color;
		context.lineWidth = 1;
		context.translate(this.x, this.y);
		context.rotate(this.angle);
		context.beginPath();
		context.moveTo(-4, -4);
		context.lineTo(8, 0);
		context.lineTo(-4, 4);
		context.closePath();
		context.fill();
		context.stroke();
		context.restore();

	};
	return obj;
};

var CannonBall = function (tower, target) {
	var obj = new Object();
	Object.extend(obj, Weapon(tower, target));
	obj.color = color(0, 0, 0);
	obj.size = 6;
	obj.splash_range = 50.0;
	obj.draw = function () {
		//SET.addLight(this.x, this.y, this.size * 3);
		context.save();
		context.lineWidth = 2;
		context.strokeStyle = "rgba(0,0,0,1)";
		context.beginPath();
		context.moveTo(this.x, this.y);
		context.arc(this.x, this.y, this.size, 0, Math.PI * 2, false);
		context.closePath();
		context.stroke();
		context.fill();
		context.restore();
	};
	return obj;
};

var Weapon = function (_tower, _target) {
	var w = new Object();
	w.x = _tower.x_mid;
	w.y = _tower.y_mid;
	w.target = _target;
	w.tower = _tower;
	w.proximity = 5;
	w.damage = _tower.damage;
	w.ps = new ParticleSystem(1, [w.x, w.y], 10, 2);
	w.last = (new Date).getTime() - start;
	w.showParticles = false;
	w.splash_range = 6;
	w.speed = 50;
	w.heatseek = false;
	w.angle = Math.atan2((_target.y - w.y), (_target.x - w.x));

	w.addExplosion = function (_x, _y) {
		var explosion = new ParticleSystem(2, [this.x, this.y], 10, 12);
		explosion.origin.x = _x;
		explosion.origin.y = _y;
		explosion.hasLifetime = true;
		explosion.update();
		assign_to_depth(explosion, SET.zIndex.particle);
	};
	w.impact = function (target) {};

	w.update = function () {

		var elapsed = 1.0 * (SET.now - this.last);
		var speed = this.speed * (elapsed / 1000);
		this.last = SET.now;
		//console.log(this.speed);
		if (this.showParticles) {
			this.ps.origin.x = this.x;
			this.ps.origin.y = this.y;
			this.ps.update();
		} else {
			this.ps = undefined
		};

		this.x += ((1 + (w.tower.level / 10)) * speed) * Math.cos(this.angle);
		this.y += ((1 + (w.tower.level / 10)) * speed) * Math.sin(this.angle);

		var creeps = SET.rendering_groups[SET.zIndex.creep];
		var l = creeps.length;
		for (var i = 0; i < l; i++) {
			var creep = creeps[i];
			var d = Math.floor(dist(this.x, this.y, creep.x, creep.y));
			if (d <= this.proximity) {
				w.impact(creep);
				creep.hp -= this.damage;
				//w.addExplosion(creep.x, creep.y);
				this.is_dead = function () {
					return true;
				};
				context.save();
				context.lineWidth = 2;
				context.strokeStyle = "rgba(0,0,0,.25)";
				context.fillStyle = color(255, 255, 255, .25);
				context.beginPath();
				context.moveTo(creep.x, creep.y);
				context.arc(creep.x, creep.y, this.splash_range, 0, Math.PI * 2, false);
				context.closePath();
				context.stroke();
				context.fill();
				context.restore();
				for (var j = 0; j < l; j++) {
					var creep = creeps[j];
					var d = Math.floor(dist(this.x, this.y, creep.x, creep.y));
					if (d <= this.splash_range && d > this.proximity) {
						creep.hp -= ((this.damage / (d / 2)) + 1);
					};
				};
				break;

			} else if (d < 50 && this.heatseek) {
				this.angle = Math.atan2((creep.y - this.y), (creep.x - this.x));
			};

		};
		if (this.x > SET.width || this.x < 0 || this.y > SET.height || this.y < 0) {
			this.is_dead = function () {
				return true;
			};
		};
		//};
	};

	w.is_dead = function () {
		return false;
	};

	return w;
};

function formalWeapon(_tower, _target) {
	this.x = _tower.x_mid;
	this.y = _tower.y_mid;
	this.target = _target;
	this.tower = _tower;
	this.proximity = 5;
	this.damage = _tower.damage;
	this.ps = new ParticleSystem(1, [w.x, w.y], 10, 2);
	this.last = (new Date).getTime() - start;
	this.showParticles = false;
	this.splash_range = 6;
	this.speed = 50;
	this.heatseek = false;
	this.angle = Math.atan2((_target.y - w.y), (_target.x - w.x));
};
formalWeapon.prototype = {
	constructor : formalWeapon,
	saveScore : function (theScoreToAdd) {
		this.quizScores.push(theScoreToAdd)
	},
	showNameAndScores : function () {
		var scores = this.quizScores.length > 0 ? this.quizScores.join(",") : "No Scores Yet";
		return this.name + " Scores: " + scores;
	},
	changeEmail : function (newEmail) {
		this.email = newEmail;
		return "New Email Saved: " + this.email;
	},
	impact : function (target) {},
	update : function () {

		var elapsed = 1.0 * (SET.now - this.last);
		var speed = this.speed * (elapsed / 1000);

		this.last = SET.now;
		//console.log(this.speed);
		if (this.showParticles) {
			this.ps.origin.x = this.x;
			this.ps.origin.y = this.y;
			this.ps.update();
		} else {
			this.ps = undefined
		};

		this.x += ((1 + (this.tower.level / 10)) * speed) * Math.cos(this.angle);
		this.y += ((1 + (this.tower.level / 10)) * speed) * Math.sin(this.angle);

		var creeps = SET.rendering_groups[SET.zIndex.creep];
		var l = creeps.length;
		for (var i = 0; i < l; i++) {
			var creep = creeps[i];
			var d = Math.floor(dist(this.x, this.y, creep.x, creep.y));
			if (d <= this.proximity) {
				this.impact(creep);
				creep.hp -= this.damage;
				//this.addExplosion(creep.x, creep.y);
				this.is_dead = function () {
					return true;
				};
				context.save();
				context.lineWidth = 2;
				context.strokeStyle = "rgba(0,0,0,.25)";
				context.fillStyle = color(255, 255, 255, .25);
				context.beginPath();
				context.moveTo(creep.x, creep.y);
				context.arc(creep.x, creep.y, this.splash_range, 0, Math.PI * 2, false);
				context.closePath();
				context.stroke();
				context.fill();
				context.restore();
				for (var j = 0; j < l; j++) {
					var creep = creeps[j];
					var d = Math.floor(dist(this.x, this.y, creep.x, creep.y));
					if (d <= this.splash_range && d > this.proximity) {
						creep.hp -= ((this.damage / (d / 2)) + 1);

					};
				};
				break;

			} else if (d < 50 && this.heatseek) {
				this.angle = Math.atan2((creep.y - this.y), (creep.x - this.x));
			};

		};
		if (this.x > SET.width || this.x < 0 || this.y > SET.height || this.y < 0) {
			this.is_dead = function () {
				return true;
			};
		};

	},
	is_dead : function () {
		return false
	},
	addExplosion : function (_x, _y) {
		var explosion = new ParticleSystem(2, [this.x, this.y], 10, 12);
		explosion.origin.x = _x;
		explosion.origin.y = _y;
		explosion.hasLifetime = true;
		explosion.update();
		assign_to_depth(explosion, SET.zIndex.particle);
	}
}

console.log(formalWeapon);
