// Projectile
(function () {
	
	//constructor
    function Projectile(options)
    {  		
		Sprite.call(this, options);
		this.thrust = 10;
		this.colId = 3;
		this.angle = 0;
		
		if(options !== undefined){ 
			if(options.thrust){
				this.thrust = options.thrust;
			}
			
			if(options.angle){
				this.angle = options.angle;
			}
			
			if(options.colId){
				this.colId = options.colId;
			}
		}
		
		this.width = 2;
		this.height = 2;
		
		this.origin.x = this.width/2;
		this.origin.y = this.height/2;
		
		this.radius = 1;
		this.type = 'projectile';
		
		if(typeof Game !== 'undefined'){
			Game.addEntity(this);
		}
	}
	
	Projectile.prototype = new Sprite();
	this.Projectile = Projectile;
	
	Projectile.prototype.hit = function(){
		this.live = false;
	}
	
	// public
	Projectile.prototype.update = function(deltaTime)
    {
		this.vel.x = (Math.cos(((this.angle)) *  Math.PI / 180) * this.thrust * deltaTime);
		this.vel.y = (Math.sin(((this.angle)) *  Math.PI / 180) * this.thrust * deltaTime);	
		
		this.pos.x += this.vel.x;
		this.pos.y += this.vel.y;
				
		if(this.pos.y < 5 || this.pos.y > Game.bounds.y + Game.bounds.height){
			this.live = false;
		}
    };
	
})();