(function () {	
	//constructor
    function Tower(options)
    {  		
		Sprite.call(this, options);
		this.width = 16;
		this.height = 16;
		this.origin = new Vector(8,8,0);
		this.clickable = true;		
		
		if(options.type){
			this.type = options.type;
		}else{
			this.type = 0;
		}
		
		if(options.tileMap){
			this.tileMap = options.tileMap;
		}else{
			this.tileMap = {};
			return false;
		}
		
		Game.TowerCount ++;
		this.radius = 8;
		
		this.angle = Math.floor(Math.random()*360);
		this.targetAngle = 0;
		this.colId = 1;
		
		this.lastFired = new Date().getTime();
		this.minFireDelay = 100;
		
		this.selected = false;
		
		Game.addEntity(this);
	}
	
	Tower.prototype = new Sprite();
	this.Tower = Tower;
	
	Tower.prototype.hit = function(hitAngle){
		// ouchie
	}
	
	
	Tower.prototype.clicked = function(){
		this.selected = true;
		// do something if clicked
	}
	
	Tower.prototype.findTarget = function(){
		var zombies = gameState.zombies,
			i = zombies.length,
			maxDist = 100,
			closestDist = maxDist+1;
		while(i--){
			var entity = zombies[i],
				dist = Game.utilities.getDistance(this.pos, entity.pos);
			if(dist < maxDist && dist < closestDist){
				closestDist = dist;
				this.target = entity;
				break;
			}	
		}
		
		this.target.selected = true;
	}
	
	Tower.prototype.update = function(deltaTime)
    {	
		Sprite.prototype.update();
			if(this.target !== undefined && this.target.live == true ){
				if(Game.utilities.getDistance(this.pos, this.target.pos) < 100){
					
					// get Tower direction
					var tx = this.target.pos.x - this.pos.x,
						ty = this.target.pos.y - this.pos.y,
						rad = Math.atan2(ty,tx);
						
					this.targetAngle = rad/Math.PI * 180;
					if(this.targetAngle > 359){
						this.targetAngle -= 359;
					}else if(this.targetAngle < 0){
						this.targetAngle = 359 + this.targetAngle;
					}
						

					if(this.targetAngle > this.angle){
						this.angle+=0.8;
					}else if(this.targetAngle < this.angle){
						this.angle-=0.8;
					}
					
					if(new Date().getTime() > this.lastFired + this.minFireDelay && this.angle > this.targetAngle-1 && this.angle < this.targetAngle+1){
						this.shoot();
					}
				}else{
					this.findTarget();
				}
			}else{
				this.findTarget();
			}
			
		if(this.angle > 359){
			this.angle -= 359;
		}else if(this.angle < 0){
				
			this.angle = 359 + this.angle;
		}
    };
	
	Tower.prototype.shoot = function(){
		this.lastFired = new Date().getTime();	
		new Projectile({x : this.pos.x, y : this.pos.y, angle : this.angle, thrust : 20, colId : this.colId});	
	}
	
	Tower.prototype.render = function(context)
    {	
		this.startY = this.height*Math.floor(this.angle/8);
		
		context.drawImage(this.resource.source,this.startX,this.startY,this.width,this.height, this.pos.x-this.origin.x, this.pos.y-this.origin.y, this.width,this.height);
		if(this.selected){
			context.strokeStyle = "rgb(0,255,0)";
			context.strokeRect(this.pos.x-this.origin.x, this.pos.y-this.origin.y, this.width, this.height);
		}
		/*if(this.radius){
			context.strokeStyle = "rgb(255,0,0)";
			context.beginPath();
			context.arc(this.pos.x, this.pos.y, this.radius, 0, Math.PI*2, true); 
			context.moveTo(this.pos.x, this.pos.y);
			context.lineTo(this.pos.x + 8 * Math.cos(this.angle*Math.PI / 180), this.pos.y + 8 * Math.sin(this.angle*Math.PI / 180)); 
			context.closePath();
			context.stroke();
		}*/
	}
})();