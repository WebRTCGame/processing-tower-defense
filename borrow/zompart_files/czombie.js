(function () {	
	//constructor
    function Zombie(options)
    {  		
		Sprite.call(this, options);
		this.width = 16;
		this.height = 18;
		this.origin = new Vector(this.width/2,this.height/2,0);
		this.clickable = true;		
		
		if(options.type){
			this.type = options.type;
		}else{
			this.type = 0;
		}
		
		if(options.tileMap){
			this.tileMap = options.tileMap;
		}else{
			this.tileMap = {};
			return false;
		}
	
		this.radius = 8;
		
		this.animSpeed = 90;
		this.lastAnim = new Date().getTime();
		this.thrust = 150;
		this.angle = Math.round(Math.random()*360);
		this.startX = 0;
		this.colId = 2;
		
		// reference for custom list
		this.list = options.list;
		
		Game.addEntity(this);
	}
	
	Zombie.prototype = new Sprite();
	this.Zombie = Zombie;
	
	Zombie.prototype.hit = function(){
		this.live = false;
	}
	
	// zombies dead remove it from the custom list
	Zombie.prototype.kill = function(){
		Sprite.prototype.kill();
		var listItem = this.list.length,
			list = this.list;
			
		while(listItem--){
			if(list[listItem] === this){
				list.splice(listItem,1);
				break;
			}
		}
	}
	
	Zombie.prototype.clicked = function(){
		// do something if clicked
	}
	
	Zombie.prototype.update = function(deltaTime)
    {	
		Sprite.prototype.update();
		//this.angle+=0.1;
		
		if(this.angle > 359){
			this.angle -= 359;
		}else if(this.angle < 0){
			this.angle = 359 - this.angle;
		}

		if(new Date().getTime() > this.lastAnim + this.animSpeed){
			this.lastAnim = new Date().getTime();
			this.startX += this.width;
		
			
			this.vel.x = (Math.cos(((this.angle)) *  Math.PI / 180) * this.thrust * deltaTime);
			this.vel.y = (Math.sin(((this.angle)) *  Math.PI / 180) * this.thrust * deltaTime);	
			
			this.pos.x += this.vel.x;
			this.pos.y += this.vel.y;
			
			if(this.startX > 32){
				this.startX = 0;
			}
			
			if(this.pos.x > Game.bounds.width+this.width/2){
				this.pos.x = 0;
			}
			
			if(this.pos.x < -this.width/2){
				this.pos.x = Game.bounds.width;
			}
			
			if(this.pos.y > Game.bounds.height+this.height/2){
				this.pos.y = 0;
			}
			
			if(this.pos.y < -this.height/2){
				this.pos.y = Game.bounds.height;
			}
			
			this.pos.z = this.pos.y;
		}

			// get zombie direction
			var tileVal = 0;
			tileVal = this.tileMap.getData(this.pos.x + this.radius * Math.cos(this.angle*Math.PI / 180), this.pos.y + this.radius * Math.sin(this.angle*Math.PI / 180));
			
			if(tileVal > 0 && tileVal < 13){
				this.angle += 180;
			}	

			//this.angle = 45*Math.round(this.angle/45);				

    };
	
	Zombie.prototype.render = function(context)
    {	
		this.startY = this.height*Math.floor(this.angle/90);
		context.drawImage(this.resource.source,this.startX,this.startY,this.width,this.height, this.pos.x-this.origin.x, this.pos.y-this.origin.y, this.width,this.height);
		
		if(this.selected){
			context.strokeStyle = "rgb(0,255,0)";
			context.strokeRect(this.pos.x-this.origin.x, this.pos.y-this.origin.y, this.width, this.height);
		}
		
		/*if(this.radius){
			context.strokeStyle = "rgb(255,0,0)";
			context.beginPath();
			context.arc(this.pos.x, this.pos.y, this.radius, 0, Math.PI*2, true); 
			context.moveTo(this.pos.x, this.pos.y);
			context.lineTo(this.pos.x + this.radius * Math.cos(this.angle*Math.PI / 180), this.pos.y + this.radius * Math.sin(this.angle*Math.PI / 180)); 
			context.closePath();
			context.stroke();
		}*/
	}
})();