(function () {	
	//constructor
    function TileMap(options)
    {  		
		this.width = Game.bounds.width;
		this.height = Game.bounds.height;
		this.visible = true;
		this.bg = true;
		this.bgIndex = 1;
	
		this.unitSize = 16;
	
		// init the tilemap data
		this.mapData = [];
		
		for(var x = 0, xLen = (this.width/this.unitSize)+2; x < xLen; x++){
			this.mapData[x] = [];
			for(var y = 0, yLen = (this.height/this.unitSize)+2; y < yLen; y++){
				this.mapData[x][y] = {type : 0};
			}
		}
		
		if(typeof options !== 'undefined'){
			if(typeof options.resource !== 'undefined'){
				this.resource = options.resource;
			}
			
			if(typeof options.bgIndex !== 'undefined'){
				this.bgIndex = options.bgIndex;
			}
		}
		this.offset = new Vector(-16,-16,0);
		Game.addEntity(this);
	}
	
	this.TileMap = TileMap;
	
	TileMap.prototype.getData = function(x,y){
		x = Math.floor((x-this.offset.x)/this.unitSize);
		y = Math.floor((y-this.offset.y)/this.unitSize);
		
		var retVal = 0;
		
		if(this.mapData[x] !== undefined && this.mapData[x][y] !== undefined && this.mapData[x][y].type !== undefined){
			retVal = this.mapData[x][y].type;
		}
		
		return retVal;
	}
	
	TileMap.prototype.loadMap = function(loadData){
		for(var x = 0; x < this.width/this.unitSize; x++){
			for(var y = 0; y < this.height/this.unitSize; y++){
				this.mapData[x][y].type = loadData[x][y];
			}
		}
	}
	
	TileMap.prototype.logData = function(){
		var mapString = "",
			arr = [];
		
		for(var x = 0; x < this.width/this.unitSize; x++){
			arr[x] = [];
			mapString += "[";
			for(var y = 0; y < this.height/this.unitSize; y++){
				arr[x][y] = this.mapData[x][y].type;
				
				mapString += this.mapData[x][y].type;
				if(y != (this.height/this.unitSize)-1){
					mapString += ","
				}
			}
			mapString += "],";
		}
		
		console.log(mapString);
		this.loadMap(arr)
	}
	
	TileMap.prototype.changeTile = function(x,y,data){
		x = Math.round((x-this.offset.x)/this.unitSize);
		y = Math.round((y-this.offset.y)/this.unitSize);
		this.mapData[x][y].type = data;
	}
	
	TileMap.prototype.checkEnclosed = function(){
		this.floodFill(0,0,0,-1);
		
		for(var x = 0; x < this.mapData.length; x++){
			for(var y = 0; y < this.mapData[0].length; y++){
				if(this.mapData[x][y].type == 0){
					this.floodFill(x,y,0,13);
				}
				
				if(this.mapData[x][y].type == -1){
					this.mapData[x][y].type =0;
				}
			}
		}
	}
	
	TileMap.prototype.floodFill = function(x, y, oldVal, newVal){
		var mapWidth = this.mapData.length,
			mapHeight = this.mapData[0].length;

		if(this.mapData[x][y].type !== oldVal){
			return true;
		}

		this.mapData[x][y].type = newVal;

		if (x > 0){ // left
			this.floodFill(x-1, y, oldVal, newVal);
		}
		if(y > 0){ // up
			this.floodFill(x, y-1, oldVal, newVal);
		}
		if(x < mapWidth-1){ // right
			this.floodFill(x+1, y, oldVal, newVal);
		}
		if(y < mapHeight-1){ // down
			this.floodFill(x, y+1, oldVal, newVal);
		}
		
		if (x > 0 && y > 0){ // up left
			this.floodFill(x-1, y-1, oldVal, newVal);
		}

		if (x < mapWidth-1 && y > 0){ // up right
			this.floodFill(x+1, y-1, oldVal, newVal);
		}
		
		if(x > 0 && y < mapHeight-1){ // down right
			this.floodFill(x-1, y+1, oldVal, newVal);
		}
		
		if(x < mapWidth-1 && y < mapHeight-1){ // down right
			this.floodFill(x+1, y+1, oldVal, newVal);
		}
	}
	
	TileMap.prototype.update = function(deltaTime)
    {	
		Sprite.prototype.update();
    };
	
	TileMap.prototype.render = function(_context){
		var width = this.mapData.length,
			height = this.mapData[0].length,
			unitSize = this.unitSize,
			offsetX = this.offset.x,
			offsetY = this.offset.y;
			
		_context.save;
		for(var x = 0; x < width; x++){
			for(var y = 0; y < height; y++){
				_context.drawImage(this.resource.source,unitSize*this.mapData[x][y].type,0,unitSize,unitSize, (x*unitSize)+offsetX, (y*unitSize)+offsetY, unitSize,unitSize);
			}
		}
		_context.restore();
	}
})();