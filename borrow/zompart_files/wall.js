(function () {	
	//constructor
    function Wall(options)
    {  		
		Sprite.call(this, options);
		this.width = 16;
		this.height = 16;
		this.origin = new Vector(8,8,0);
		this.clickable = true;		
		
		if(options.type){
			this.type = options.type;
		}else{
			this.type = 1;
		}
		
		this.pos.x = Math.round(this.x/16)*16;
		this.pos.y = Math.round(this.y/16)*16;

		Game.addEntity(this);
	}
	
	Wall.prototype = new Sprite();
	this.Wall = Wall;
	
	Wall.prototype.hit = function(hitAngle){

	}
	
	Wall.prototype.clicked = function(){
		// do something if clicked
	}
	
	Wall.prototype.update = function(deltaTime)
    {	
		Sprite.prototype.update();

    };
})();