(function(){
var seed = 'jason';
	
	if(window.location.href.split('?')[1]){
		seed = window.location.href.split('?')[1];
	}
	Math.seedrandom(seed);
	function ZomPart(){
		this.step = 100;
		this.delStep = 0;
	};
	
	ZomPart.prototype = new jGame({"canvas" : "playCanvas", "width": 800, "height" : 600, "frameRate" : Math.ceil(1000/120), "showFrameRate" : true});
	this.ZomPart = ZomPart;
	
	ZomPart.prototype.init = function(){
		// Call the main game constructor
		jGame.prototype.init.call(this);
		
		// Create the background
		this.resourceManager.add("images/graphics.png", 1, "graphics");
		this.resourceManager.add("images/splash.png", 1, "menuSplash");
		this.resourceManager.add("images/grassbg4.png", 1, "grass1");
		this.resourceManager.add("images/walls.png", 1, "walls");
		this.resourceManager.add("images/tower.png", 1, "tower");
		this.resourceManager.add("images/orcbase.png", 1, "fastMeleeZombie");
				
		this.loaded(this);
	}
	
	// overriding the load prototype
	ZomPart.prototype.loaded = function(game){
		var self = this;
		if(this.resourceManager.loadingComplete){
			game.afterLoad();
			return true;
		}else{
			setTimeout(function(){self.loaded(game)},100);
			return false;
		}
		event.time;
	}
	
	// After the random seed is generated do the rest.
	// Sets up the gamestate
	ZomPart.prototype.afterLoad = function(){
		//Game.utilities.preCalcRotation(this.resourceManager.getResource("fastMeleeZombie"), 45, 32, 32, 270);
		//this.resourceManager.add(Game.utilities.colorize(this.resourceManager.getResource("fastMeleeZombie"),{h:100,s:0,v:0}), 1, "greenZombie");
		
		Game.utilities.preCalcRotation(this.resourceManager.getResource("tower"), 45, 16, 16, 270);
	
		this.gameState = {};
		gameState = this.gameState;
		
		this.gameHud = {};
		var gameHud = this.gameHud;
		
		gameHud.ui = new UI();
		gameHud.ui.zombieCount = new Label({'text':' ', x:0,y:50, 'font':'14pt arial bold'});
		gameHud.ui.addItem(gameHud.ui.zombieCount, 100);	
	
		// Make the ui
		gameState.ui = new UI();
		gameState.cursor = new Cursor({
									width:16, 
									height:16, 
									startX : 496, 
									resource : this.resourceManager.getResource('graphics')
									});
									
		gameState.ui.addItem(gameState.cursor, 100);
		
		gameState.saveButton = new Button({
										width:20, 
										height:20, 
										x : 10, 
										y : 420,
											clicked : function(){
												gameState.tileMap.logData();
											}});
											
		gameState.ui.addItem(gameState.saveButton);
		
		gameState.loadButton = new Button({width:20, 
											height:20, 
											x : 40, 
											y : 420,
									clicked : function(){
										gameState.tileMap.loadMap(new LevelData().level1.mapData);
									}});
									
		gameState.ui.addItem(gameState.loadButton);
		
		// Add the gamestates graphics
		var grassBg = new Background({'resource' : this.resourceManager.getResource('grass1'), 'bgIndex' : 0}),
			parralaxBackground = new ParralaxBackground();
			parralaxBackground.addBackground({'background' : grassBg, 'speedMultX' : 0, 'speedMultY' : 0});
		
		gameState.tileMap = new TileMap({resource : Game.resourceManager.getResource("walls"), bgIndex : 2});
		gameState.zombies = [];
		gameState.towers = [];
		gameState.phase = 0;
		
		for(var i = 0; i < 250; i++){
			gameState.zombies.push(new Zombie({
									x:Math.random()*Game.bounds.width, 
									y:Math.random()*Game.bounds.height, 
									resource : Game.resourceManager.getResource("fastMeleeZombie"), 
									tileMap : gameState.tileMap,
									list : gameState.zombies
									}));
		}
	
		for(var i = 0; i < 1; i++){
			gameState.towers.push(new Tower({x:(Math.random()*160)+160, y:(Math.random()*120)+120, resource : Game.resourceManager.getResource("tower"), tileMap : gameState.tileMap}));
		}
		
		this.setupMenu();
		this.setupHowToPlay();
		
		requestAnimFrame( function(){Game.update()} );
	}
	
	// Setup the menu state
	ZomPart.prototype.setupMenu = function(){
		Game.addState("menu");
		Game.switchState({name : "menu"});

		var menuState = {};
		
		menuState.ui = new UI();
				
		var splash = new Background({'resource' : this.resourceManager.getResource('menuSplash'), 'bgIndex' : 0}),
			parralaxBackground = new ParralaxBackground({'state' : {name : "menu"}});
			parralaxBackground.addBackground({'background' : splash, 'speedMultX' : 0, 'speedMultY' : 0});
	

		menuState.startButton = new Button({width:158, height:60, x : Game.width/2-146/2, y : 270, resource : this.resourceManager.getResource('graphics'),
											clicked : function(){
												Game.switchState({id : 0, enterTransition : {effect : 'fadeIn'}, exitTransition : {effect : 'fadeOut'}});
											},
											hover : function(over){
												if(over){
													this.startY = 60;
												}else{
													this.startY = 0;
												}
											}});
											
		menuState.ui.addItem(menuState.startButton);
		
		menuState.htpButton = new Button({width:158, height:60, x : Game.width/2-146/2, y : 330, resource : this.resourceManager.getResource('graphics'),
											clicked : function(){
												Game.switchState({name : "htplay", enterTransition : {effect : 'fadeIn'}, exitTransition : {effect : 'fadeOut'}});
											},
											hover : function(over){
												if(over){
													this.startY = 60;
												}else{
													this.startY = 0;
												}
											}});
											
		menuState.ui.addItem(menuState.htpButton);
		
		menuState.cursor = new Cursor({width:16, height:16, startX : 496, resource : this.resourceManager.getResource('graphics')});
		menuState.ui.addItem(menuState.cursor, 100);
	}
	
	ZomPart.prototype.setupHowToPlay = function(){
		Game.addState("htplay");

		var howToPlayObjects = {},
			htpState = Game.getState({name:"htplay"});
		
		howToPlayObjects.ui = new UI({state : htpState});		

		
		howToPlayObjects.backButton = new Button({width:158, height:60, x : 10, y : 420, resource : this.resourceManager.getResource('graphics'),
											clicked : function(){
												Game.switchState({name : "menu", enterTransition : {effect : 'fadeIn'}, exitTransition : {effect : 'fadeOut'}});
											},
											hover : function(over){
												if(over){
													this.startY = 60;
												}else{
													this.startY = 0;
												}
											}});
											
		howToPlayObjects.ui.addItem(howToPlayObjects.backButton);
		
		
		howToPlayObjects.cursor = new Cursor({width:16, height:16, startX : 496, resource : this.resourceManager.getResource('graphics')});
		howToPlayObjects.ui.addItem(howToPlayObjects.cursor, 100);
	}
	
	ZomPart.prototype.update = function(){
		jGame.prototype.update.call(this);
		this.gameHud.ui.zombieCount.text = this.gameState.zombies.length;
		
		var entities = this.entities,
			entLen = this.entities.length;
		
			
		for(var i = 0; i < entLen; i++){
			if(entities[i].colId === undefined){
				continue;
			}
			
			var curItem = entities[i];
						
			for(var j = 0; j < entLen; j++){
				if(entities[j].colId === undefined){
					continue;
				}
				var item = entities[j];
				
				// Only check if the projectile is not owned by the colliding object/colliding object isnt a projectile/item is not itself				
				if(curItem === item || curItem.colId === item.colId || curItem.type === 'projectile'){
					continue;
				}
				
				var checkRadius = curItem.radius + item.radius;
				
				if(this.utilities.getDistance(curItem.pos, item.pos) < checkRadius){
					curItem.hit();
					item.hit();
					//if(item.type !== 'ship'){
					//	item.hit();
					//}
					
					break;
				}
			}
		}
	}
	
	ZomPart.prototype.clicked = function(event, self){
		jGame.prototype.clicked(event,self);
		if(this.currentState.id === 0){
			if(gameState.phase == 0){
				gameState.tileMap.changeTile(this.cX, this.cY, 5);
				gameState.tileMap.checkEnclosed();
			}else{
				gameState.zombies.push(new Zombie({
										x:this.cX, 
										y:this.cY, 
										resource : Game.resourceManager.getResource("fastMeleeZombie"),
										tileMap : gameState.tileMap,
										list : gameState.zombies
									}));
			}
		}
	}

})();

var Game = {};
	
window.onload = function(){
	Game = new ZomPart();
	Game.init();	
};