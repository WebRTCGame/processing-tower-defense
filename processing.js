(function () {

	this.Processing = function Processing(aElement) {
		if (typeof aElement === "string")
			aElement = document.getElementById(aElement);

		var p = buildProcessing(aElement);

		return p;
	};

	function buildProcessing(curElement) {

		var p = {};

		// mouseButton constants: values adjusted to come directly from e.which
		p.LEFT = 1;
		p.CENTER = 2;
		p.RIGHT = 3;

		// "Private" variables used to maintain state
		var curContext = curElement.getContext("2d");
		var mousePressed = false;
		var keyPressed = false;
		var firstX,
		firstY,
		secondX,
		secondY,
		prevX,
		prevY;

		p.pmouseX = 0;
		p.pmouseY = 0;
		p.mouseX = 0;
		p.mouseY = 0;
		p.mouseButton = 0;

		// Will be replaced by the user, most likely
		p.mouseDragged = undefined;
		p.mouseMoved = undefined;
		p.mousePressed = undefined;
		p.mouseReleased = undefined;
		p.keyPressed = undefined;
		p.keyReleased = undefined;

		// The height/width of the canvas
		p.width = curElement.width - 0;
		p.height = curElement.height - 0;

		p.init = function init(code) {

			attach(curElement, "mousemove", function (e) {
				var scrollX = window.scrollX != null ? window.scrollX : window.pageXOffset;
				var scrollY = window.scrollY != null ? window.scrollY : window.pageYOffset;
				p.pmouseX = p.mouseX;
				p.pmouseY = p.mouseY;
				p.mouseX = e.clientX - curElement.offsetLeft + scrollX;
				p.mouseY = e.clientY - curElement.offsetTop + scrollY;

				if (p.mouseMoved) {
					p.mouseMoved();
				}

				if (mousePressed && p.mouseDragged) {
					p.mouseDragged();
				}
			});

			attach(curElement, "mousedown", function (e) {
				mousePressed = true;
				p.mouseButton = e.which;

				if (typeof p.mousePressed === "function") {
					p.mousePressed();
				} else {
					p.mousePressed = true;
				}
			});

			attach(curElement, "contextmenu", function (e) {
				e.preventDefault();
				e.stopPropagation();
			});

			attach(curElement, "mouseup", function (e) {
				mousePressed = false;

				if (typeof p.mousePressed != "function") {
					p.mousePressed = false;
				}

				if (p.mouseReleased) {
					p.mouseReleased();
				}
			});

			attach(document, "keydown", function (e) {
				keyPressed = true;

				p.key = e.keyCode + 32;

				if (e.shiftKey) {
					p.key = String.fromCharCode(p.key).toUpperCase().charCodeAt(0);
				}

				if (typeof p.keyPressed === "function") {
					p.keyPressed();
				} else {
					p.keyPressed = true;
				}
			});

			attach(document, "keyup", function (e) {
				keyPressed = false;

				if (typeof p.keyPressed != "function") {
					p.keyPressed = false;
				}

				if (p.keyReleased) {
					p.keyReleased();
				}
			});

			function attach(elem, type, fn) {
				if (elem.addEventListener)
					elem.addEventListener(type, fn, false);
				else
					elem.attachEvent("on" + type, fn);
			}
		};

		return p;
	}

})();
